# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
``` 
Answer1: 2,1. setTimeout is a 'macrotask', it is going to a callback queue(even if delay is 0) first (to the Browser API), then '2' will log, then console.log('1') will execute

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
Answer2: integers from 0 to 10. It is recursion. Function calls itself every time as long as condition (d < 10) is met and each console.log from 0 to 9 goes to callstack. When finally condition is wrong (10<10) console log(10) is executed, and then LIFO execution starts. 9 -> 0

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
Answer3: There might be a problem if we provide false to the 'foo', because || returns the first true and that will be 5.
If we provide an empty arrow or object, it will return arrow or object as they are truthy even when empty. Also 0 is falsy. I would add conditions that d is not equal to {},[], 0  or false

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
Answer4: output: 3. 'function' will look for 'a', it won't find it inside, then it will jump out to 'foo' and take a from closure.

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```
Answer5: Function double would be used for executing a function that awaits 'a' with a 0.1 second delay. Such as:
function log(b) {
    console.log(b)
}
double(8, log)